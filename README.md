One Implementation of FA2 in SmartPy
====================================

The actual implementation is at:
[`multi_asset.py`](./multi_asset.py), this repository also
contains testing, benchmarking, and basic usage tooling.

See also the `./michelson/` for precompiled versions of the contract.


Build/Basic Usage
-----------------

### Dependencies

Check and install dependencies:

    ./please.sh prepare

If you have an opam-switch you want to reuse instead of creating one use the
following command before `prepare`:

    opam switch link NAME .


The script `please.sh` can also install its own version of `smartpy-cli`:

    ./please.sh get_smartpy

It installs the currently recommended version (see
`./please.sh get_smartpy_recommended_version`) at a local path (see
`./please.sh get_smartpy_local_install_path`).

### Build

Then build:

    ./please.sh build

The command `./please.sh install` installs the binary “`fatoo`” at `$PREFIX/bin`
(default: `/usr/bin`).

### Run SmartPy Simulations

Simulations for some configurations fail as of now (bugs/missing things in
SmartPy):

    ./please.sh run simulations

### Docker Images

To build the images yourself, run:

```
$ docker build -t <imagename> --target <setup|build|run> . \
  --build-arg flextesa_build_image --build-arg smartpy_version
```

To get the required env variables, run `./scripts/dockerhelper.sh get_build_envs`

Or you may use the prebuilt images, for many commit hashes, e.g. `hash=e803c69c`, the following docker images are
available:

- `registry.gitlab.com/smondet/fa2-smartpy:$hash-setup`: contains all the
  dependencies required to build.
- `registry.gitlab.com/smondet/fa2-smartpy:$hash-build`: contains all the
  dependencies required to build **and** the project itself, built.
- `registry.gitlab.com/smondet/fa2-smartpy:$hash-run`: contains only the
  required binaries to run everything (this project + tezos + flextesa).

One may try:

    docker run --rm registry.gitlab.com/smondet/fa2-smartpy:$hash-run fatoo --help

The most recent docker images ( master branch ) are also available at

* `registry.gitlab.com/smondet/fa2-smartpy:master-run`
* `registry.gitlab.com/smondet/fa2-smartpy:master-build`
* `registry.gitlab.com/smondet/fa2-smartpy:master-setup`