open! Base
open! Common
open Io.Monad_infix

let fatoo_gitlab = "https://gitlab.com/smondet/fa2-smartpy/"
let fatoo_gitlab_issues = "https://gitlab.com/smondet/fa2-smartpy/-/issues"
let all_tz_contracts = fatoo_gitlab // "-/tree/master/michelson"

let fa2_spec_md =
  "https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md"

let default_tz =
  fatoo_gitlab
  // "-/raw/8bdeced/michelson/20200615-162614+0000_e1e6c44_contract.tz"

let intro_agora_post =
  "https://forum.tezosagora.org/t/implementing-fa2-an-update-on-the-fa2-specification-and-smartpy-implementation-release/1870"

let fatoo_multi_asset_dot_py = fatoo_gitlab // "-/blob/master/multi_asset.py"
let docker_image_commit = "228dcfe9"

let docker_image =
  Fmt.str "registry.gitlab.com/smondet/fa2-smartpy:%s-run" docker_image_commit

let alice_pkh = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"

let alice_sk =
  "unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"

let bob_pkh = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"

let bob_sk =
  "unencrypted:edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt"

let get_balance_script =
  String.strip
    {mich|
parameter
    (pair (pair (address %administrator)
                (pair (nat %all_tokens) (big_map %ledger (pair address nat) nat)))
          (pair (pair (unit %version_20200615_tzip_a57dfe86_contract)
                      (big_map %operators (pair (address %owner) (address %operator)) unit))
                (pair (bool %paused)
                      (big_map %tokens
                         nat
                         (pair (pair %metadata
                                  (nat %token_id)
                                  (pair (string %symbol)
                                        (pair (string %name) (pair (nat %decimals) (map %extras string string)))))
                               (nat %total_supply)))))) ;
storage unit;
code
 {
    CAR ; # Get parameter
    CAR ; # Get the pair (admin , _)
    CDR ; # Get the pair (all_token, ledger)
    CDR ; # Get %ledger
    PUSH (pair address nat) (Pair "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" 0);
    GET ; # Get the value in the ledger at the above key
    FAILWITH
 };
|mich}

let sandbox_tutorial_url = "https://assets.tqtezos.com/docs/setup/2-sandbox"

let fatoo_docker_command fatoo_client_uri =
  Fmt.str
    "docker run -it -u \"$UID\" --network host -v \"$PWD:/work\" -w /work --rm \
     -e fatoo_client=%S --entrypoint fatoo %s"
    fatoo_client_uri docker_image

let make_tutorial ~header ?(skip_basic_section = false) () =
  let open Markdown in
  let indented_command exec l =
    let indent = String.length exec + 1 in
    exec ^ " "
    ^ String.concat
        ~sep:(Fmt.str " \\\n%s" (String.make indent ' '))
        (List.map l ~f:(Fmt.str "%s")) in
  let import_sk name sk =
    indented_command "tezos-client"
      [Fmt.str "import secret key %s" name; sk; "--force"] in
  let _todo s = par ("<b style=\"color: #900\">TODO:</b>" %% s) in
  let _wip s = par ("<b style=\"color: #009\">WIP:</b>" %% s) in
  let basic_section () =
    section "Basic Usage With tezos-client"
      [ Fmt.kstr par
          "This assumes you have `tezos-client` properly set up to talk to \
           Carthagenet or to a “full” [sandbox](%s) (i.e. with bakers)."
          sandbox_tutorial_url
      ; par
          "This part requires 4 accounts with a few ꜩ imported into \
           `tezos-client`, as `administrator`, `originator`, `alice` and \
           `bob`."
      ; par
          "In the case of the sandbox tutorial we use `alice` also as \
           `originator` and `administrator`:"
      ; command_bloc
          ~how:(`Code_block [`Command])
          (String.concat ~sep:"\n"
             [ import_sk "alice" alice_sk; import_sk "originator" alice_sk
             ; import_sk "administrator" alice_sk; import_sk "bob" bob_sk ])
      ; section "Get The Michelson Code"
          [ par
              ( "FA2-SmartPy uses SmartPy's meta-programming facilities to \
                 provide more than one Michelson contract, a.k.a. \
                 *“builds.”*." %% "A few of the builds are available at"
              %% uri all_tz_contracts
              % ", see [below](#originate) for a description of the various \
                 builds." ); par "Let's download the “default” one:"
          ; command_bloc
              (indented_command "wget"
                 ["-O fa2_default.tz"; Fmt.str "'%s'" default_tz]) ]
      ; section "Origination"
          (let init ?(indent = false) adm nbtok ldgr ops pause toks =
             let indented =
               Fmt.str
                 "(Pair\n\
                 \   (Pair %S (Pair %s %s))\n\
                 \   (Pair (Pair Unit %s)\n\
                 \         (Pair %s %s)))" adm nbtok ldgr ops pause toks in
             if indent then indented
             else
               String.filter ~f:(function '\n' -> false | _ -> true) indented
               |> String.split ~on:' '
               |> List.filter ~f:(function "" -> false | _ -> true)
               |> String.concat ~sep:" " in
           [ par
               ( "Origination works like for any contract,"
               %% "we need the above code, a few ꜩ, and a michelson \
                   expression to initialize the storage."
               %% "In our case, it should look like:" )
           ; code_block ~lang:"ocaml"
               ( init ~indent:true "<admin-pkh>" "<nb-of-tokens>"
                   "<ledger-big-map>" "<operators-big-set>" "<paused>"
                   "<tokens-big-map>"
               |> String.split ~on:'\n' )
             (* ; command_bloc ~how:[`Stdout None]
                "fatoo make-init '<put-admin-address-here>'" *)
           ; par
               "It is expected that `<nb-of-tokens>` is the cardinal of the \
                `<tokens-big-map>` map, and that only “known” tokens are \
                used in the `<ledger-big-map>` big-map. To maintain all \
                invariants properly, it is recommended to initialize the \
                storage empty, and use the `%mint` entrypoint to fill the \
                contract."
           ; par
               "Let's originate such an unpaused empty contract while setting \
                the `administrator` address:"
           ; (let init = init alice_pkh "0" "{}" "{}" "False" "{}" in
              let cmd =
                indented_command "tezos-client"
                  [ "originate contract myfa2"; "transferring 0 from originator"
                  ; "running fa2_default.tz"
                  ; Fmt.str "--init %s" (Path.quote init); "--burn-cap 10"
                  ; "--force --no-print-source" ] in
              command_bloc
                ~how:(`Code_block [`Command; `Stdout (Some (7, 8))])
                cmd) ])
      ; section "Mint"
          (let make_arg addr amount symb id =
             Fmt.str "(Pair (Pair %S %s) (Pair %S %s))" addr amount symb id
           in
           [ par
               "Here we want to make a transfer “as” the `administrator` \
                set in the previous section."
           ; par
               "The minting entry-point is not standardized in the FA2 \
                specification, for fa2-smartpy it should look like this:"
           ; code_block ~lang:"ocaml"
               [make_arg "<address>" "<amount>" "<token-symbol>" "<token-id>"]
           ; Fmt.kstr par
               "The default build assumes that token-IDs are consecutive \
                natural numbers (0, 1, 2, …), if because of some particular \
                constraint the user requires arbitrary token-IDs there is a \
                build option in FA2-SmartPy to generate such a contract (see \
                [documentation](%s))."
               fatoo_gitlab
           ; par
               "For instance, let's, as `administrator`, mint 100 `TK0` tokens \
                to `alice`:"
           ; (let arg = make_arg alice_pkh "100" "TK0" "0" in
              let cmd =
                indented_command "tezos-client"
                  [ "transfer 0 from administrator to myfa2"; "--entrypoint mint"
                  ; Fmt.str "--arg %s" (Path.quote arg); "--burn-cap 3" ] in
              command_bloc
                ~how:(`Code_block [`Command; `Stdout (Some (7, 8))])
                cmd) ])
      ; section "Transfer"
          (let txs_item (t, a, o) = Fmt.str "Pair %S (Pair %s %s)" t o a in
           let transfer_item f txs =
             Fmt.str "Pair %S {%s}" f
               (String.concat ~sep:" ; " (List.map txs ~f:txs_item)) in
           let indented_transfer f txs =
             Fmt.str "  %s ;" (transfer_item f txs) in
           [ par
               "The transfer entry-point in FA2 is “batched” at two levels \
                i.e. one contract call contains a list of transfer elements, \
                each transfer element is a “from-address” and a list of \
                outgoing transactions:"
           ; code_block
               [ "{"
               ; indented_transfer "<from-1>"
                   [("<to-1>", "<amount-1>", "<token-id-1>")]
               ; indented_transfer "<from-2>"
                   [ ("<to-2>", "<amount-2>", "<token-id-2>")
                   ; ("<to-3>", "<amount-3>", "<token-id-3>") ]; "  ..."; "}" ]
           ; par "Here we, as `alice`, transfer 5 of our 100 TK0 to `bob`:"
           ; (let arg =
                Fmt.str "{ %s }" (transfer_item alice_pkh [(bob_pkh, "5", "0")])
              in
              let cmd =
                indented_command "tezos-client"
                  [ "transfer 0 from alice to myfa2"; "--entrypoint transfer"
                  ; Fmt.str "--arg %s" (Path.quote arg); "--burn-cap 3" ] in
              command_bloc
                ~how:(`Code_block [`Command; `Stdout (Some (8, 8))])
                cmd) ])
      ; section "Get Balance Off-Chain"
          [ par
              "As an example of interaction with big-maps in the contract's \
               storage using Michelson and `tezos-client`, here we obtain \
               `alice`'s balance of TK0 tokens."
          ; par
              "We need a script which takes the contract's storage type as \
               parameter (literally copy-pasted), and uses Michelson to \
               extract the value in the `%ledger` big-map; in this case we \
               just display it with the `FAILWITH` instruction, but one could \
               do much more, including putting in storage (left as exercise \
               for the reader ☺). Let's save it as `get-balance.tz`:"
          ; code_block ~lang:"ocaml" (String.split ~on:'\n' get_balance_script)
          ; Fmt.kstr silent_command "printf '%%s' %s > get-balance.tz"
              (Path.quote get_balance_script)
          ; par
              "In this case, we expect the `tezos-client` command to fail, \
               since we want to read the error message:"
          ; command_bloc ~failure_expected:true
              ~how:(`Code_block [`Command; `Stderr (Some (0, 8))])
              (indented_command "tezos-client"
                 [ "run script get-balance.tz on storage Unit"; "and input"
                 ; "\"$(tezos-client get contract storage for myfa2)\"" ])
          ; par
              "We can *clearly* see in the error value (passed to `FAILWITH`) \
               that `alice`'s balance is 95 TK0 (100 minted *minus* 5 \
               transferred to `bob`)." ] ] in
  let fatoo_section () =
    let fatoo_client_uri = Fmt.str "http://:20000/%s?wait=0" alice_sk in
    let export_client_config =
      Fmt.str "export fatoo_client='%s'" fatoo_client_uri in
    let pkh_of_csv csv = Fmt.str "$(cut -d, -f 3 %s)" csv in
    let sk_of_csv csv = Fmt.str "$(cut -d, -f 4 %s)" csv in
    let pkh_of_name name = Fmt.str "${%s_pkh}" name in
    let sk_of_name name = Fmt.str "${%s_sk}" name in
    let keys =
      [ ("admin", "the-only-administrator-of-the-contract")
      ; ("owner0", "the-0th-aka-first-owner"); ("owner1", "ready-owner-one")
      ; ("owner2", "this-is-a-potential-token-owner-too") ] in
    let operator_key = ("operator", "youve-been-operated-ill-be-back") in
    let origination_choice = "mutran_contract" in
    let contract_address_arg =
      Fmt.str "\"$(cat kt1_%s.txt)\"" origination_choice in
    let key_of_seed (short_name, mnemonic) =
      indented_command "fatoo"
        [ "account-of-seed"; Fmt.str "%S" mnemonic
        ; Fmt.str "--output %s.csv" short_name ] in
    let define_sk_pkh_for_key (name, _) =
      let csv = name ^ ".csv" in
      [ Fmt.str "export %s_pkh=\"%s\"" name (pkh_of_csv csv)
      ; Fmt.str "export %s_sk=\"%s\"" name (sk_of_csv csv) ] in
    let hidden_setup =
      [export_client_config]
      @ List.concat_map ~f:define_sk_pkh_for_key (operator_key :: keys) in
    let fatoo ?how ?failure_expected args =
      command_bloc ~hidden_setup ?how ?failure_expected
        (indented_command "fatoo" args) in
    let show_contract ?(also = []) owners =
      fatoo ~how:command_and_stderr
        ( Fmt.str "show-storage %s" contract_address_arg
          :: List.map owners ~f:(fun i ->
                 Fmt.str "--known-address %S"
                   (Fmt.kstr pkh_of_csv "owner%d.csv" i))
        @ List.map also ~f:(fun f ->
              Fmt.str "--known-address %S" (Fmt.kstr pkh_of_csv "%s.csv" f)) )
    in
    let transfer_arg f t a k =
      Fmt.str "\"from:%s to:%s amount: %d token: %d\"" f t a k in
    let transfer_arg_of_names f t a k =
      transfer_arg
        (Fmt.kstr pkh_of_name "%s" f)
        (Fmt.kstr pkh_of_name "%s" t)
        a k in
    section "The `fatoo` Application"
      [ section "Obtain and Setup Client"
          [ par
              "In this section we use the `fatoo` command line interface to \
               some *builds* of FA2-SmartPy. You need `fatoo` installed in \
               your `$PATH` or you may use Docker:"
          ; command_bloc
              ~failure_expected:true (* In case docker is not available. *)
              ~how:(`Code_block [`Command])
              (Fmt.str
                 "fatoo --version\n\
                  # or:\n\
                  docker run -it --rm --entrypoint fatoo %s --version"
                 docker_image)
          ; Fmt.kstr par
              "The `fatoo` application has many commands, see \
               `fatoo [subcommand] --help`. At the same time, it is \
               work-in-progress, so feel free to submit issues and feature \
               requests in the main [repository](%s)."
              fatoo_gitlab
          ; par "Two environment variables can be used to configure"
          ; itemize
              [ "`fatoo_root_path`: logs, outputs"
              ; "`fatoo_client`: the more important one, it is an URI \
                 describing how to configure `tezos-client` and talk to the \
                 node:" ]
          ; par "See command `fatoo show-client-uri-documentation`:"
          ; command_bloc ~how:`Quoted_stdout "fatoo show-client-uri-doc"
          ; Fmt.kstr par
              "Assuming we are using the [sandbox](%s) setup, we can configure \
               the client using `alice`'s private key as follows:"
              sandbox_tutorial_url
          ; code_block ~lang:"sh"
              [ export_client_config; ""; "# Or, for docker, use:"; ""
              ; Fmt.str "alias fatoo='%s'"
                  (fatoo_docker_command fatoo_client_uri) ]
          ; par
              "The application has a `client` subcommand which just calls \
               `tezos-client` properly, one may test their setup with:"
          ; fatoo ["client bootstrapped"] ]
      ; section "Setup Accounts"
          [ par
              "Here we create four key-pairs from mnemonic seeds, to be used \
               in the following sections:"
          ; command_bloc
              (String.concat ~sep:"\n" (List.map keys ~f:key_of_seed))
          ; Fmt.kstr par
              "The resulting CSVs are in the same format as with \
               [flextesa](%s), they contain: `<phrase>,<pk>,<pkh>,<sk>` see \
               for instance:"
              "https://tezos.gitlab.io/flextesa/"
          ; command_bloc
              (Fmt.str "echo \"Public key hash: %s\"\necho \"Secret key: %s\""
                 (pkh_of_csv "admin.csv") (sk_of_csv "admin.csv"))
          ; par "Let's name all of these:"
          ; command_bloc
              (String.concat ~sep:"\n"
                 (List.concat_map keys ~f:define_sk_pkh_for_key)) ]
      ; section "Originate"
          [ par
              "The application contains the code for a few variants of the \
               contract:"
          ; fatoo
              ~how:(`Code_block [`Command; `Stdout None])
              [ "list-contract-variants"
              ; "--details description --format markdown" ]
          ; Fmt.kstr par
              "One can dump the Michelson code into a file (see \
               `fatoo get-code --help`), but there is no need since one can \
               directly originate contracts from the application. Let's \
               originate `%s`, the full blown FA2 implementation with an extra \
               entry-point which allows the administrator to transfer funds \
               which may potentially end-up in the contract's balance."
              origination_choice
          ; fatoo
              ~how:(`Code_block [`Command; `Stdout None; `Stderr None])
              [ Fmt.str "originate %s" origination_choice
              ; Fmt.str "--administrator %S" (pkh_of_name "admin")
              ; Fmt.str "--output-address kt1_%s.txt" origination_choice ]
          ; par "The command has saved the contract address in the file:"
          ; command_bloc (Fmt.str "cat kt1_%s.txt" origination_choice)
          ; par
              "And we can already display the state of the contract (storage):"
          ; show_contract []
            (* ; fatoo
                ~how:(`Code_block [`Command; `Stderr None])
                [Fmt.str "show-storage %s" contract_address_arg] *) ]
      ; section "Mint and Multi-Transfer"
          [ par
              "In order to mint tokens, the administrator needs to be able to \
               call the contract on chain, for this we need to transfer at \
               least a few μꜩ to that address. One can use `tezos-client` \
               but `fatoo` has shortcut command to transfer from the \
               configured “funding” account (amounts are in `mutez`):"
          ; fatoo
              ~how:(`Code_block [`Command; `Stderr None])
              ["fund-address"; Fmt.str "%S" (pkh_of_name "admin"); "10_000_000"]
          ; par
              "Note that for now `owner0` does not exist on chain, we're still \
               minting tokens to them:"
          ; fatoo
              [ Fmt.str "call-mint --token-id 0 --token-symbol TQ0"
              ; Fmt.str "%S 1_000_000" (pkh_of_name "owner0")
              ; Fmt.str "--source %S" (sk_of_name "admin")
              ; Fmt.str "--address %s" contract_address_arg ]
          ; par "Let's add another token `TQ1` still minting some to `owner0`:"
          ; fatoo
              [ Fmt.str "call-mint --token-id 1 --token-symbol TQ1"
              ; Fmt.str "%S 2_000" (pkh_of_name "owner0")
              ; Fmt.str "--source %S" (sk_of_name "admin")
              ; Fmt.str "--address %s" contract_address_arg ]
          ; par
              "Let's see the storage; we see the new tokens `TQ0` and `TQ1` \
               and, since we provide a “known token owner” on the \
               command-line, we can see their balances:"
          ; show_contract [0]
            (* fatoo
               ~how:(`Code_block [`Command; `Stderr None])
               [ Fmt.str "show-storage %s" contract_address_arg
               ; Fmt.str "--known-address %S" (pkh_of_name "owner0") ] *)
          ; par
              "Now let's get `owner0` to do a batch-transfer. First, we need \
               to feed some gas to that address:"
          ; fatoo
              ~how:(`Code_block [`Command; `Stderr None])
              ["fund-address"; Fmt.str "%S" (pkh_of_name "owner0"); "1_000_000"]
          ; par
              "Then, since the token-owner can do self-transfer we use \
               `owner1`'s secret-key to transfer TQ0s and TQ1s to `owner1` and \
               `owner2`:"
          ; fatoo
              [ "call-transfer"; transfer_arg_of_names "owner0" "owner1" 10 0
              ; transfer_arg_of_names "owner0" "owner1" 100 1
              ; transfer_arg_of_names "owner0" "owner2" 10 1
              ; Fmt.str "--source %S" (sk_of_name "owner0")
              ; Fmt.str "--address %s" contract_address_arg ]
          ; par "We can then observe the resulting state:"
          ; show_contract [0; 1; 2] ]
      ; section "Using Operators"
          [ par "Let's create an `operator` key-pair:"
          ; command_bloc
              ~how:(`Code_block [`Command])
              (String.concat ~sep:"\n"
                 (key_of_seed operator_key :: define_sk_pkh_for_key operator_key))
          ; par
              "We will now get all the owners to delegate to “operator,” \
               see also the command `fatoo call-update-operators --help`:"
          ; command_bloc ~hidden_setup
              ~how:(`Code_block [`Command])
              (let fund owner =
                 indented_command "fatoo"
                   [ "fund-address"
                   ; Fmt.str "%S" (Fmt.kstr pkh_of_name "owner%d" owner)
                   ; "1_000_000" ] in
               let add_opt owner =
                 indented_command "fatoo"
                   [ "call-update-operators"
                   ; Fmt.str "\"add@@ operator: %s owner: %s\""
                       (pkh_of_name "operator")
                       (Fmt.kstr pkh_of_name "owner%d" owner)
                   ; Fmt.str "--source %S" (Fmt.kstr sk_of_name "owner%d" owner)
                   ; Fmt.str "--address %s" contract_address_arg ] in
               String.concat ~sep:"\n"
                 [add_opt 0; fund 1; add_opt 1; fund 2; add_opt 2])
          ; par
              "We see that now, the same operator is present in every account:"
          ; show_contract ~also:["operator"] [0; 1; 2]
          ; par
              "Finally, let's get `operator` to run a *batch-transfer-heist* \
               of all the tokens:"
          ; fatoo ~how:command_and_stdall
              [ "fund-address"; Fmt.str "%S" (pkh_of_name "operator")
              ; "2_000_000_000" ]
          ; fatoo ~how:command_and_stdall
              [ "call-transfer"
              ; transfer_arg_of_names "owner0" "operator" 999990 0
              ; transfer_arg_of_names "owner0" "operator" 1890 1
              ; transfer_arg_of_names "owner1" "operator" 10 0
              ; transfer_arg_of_names "owner1" "operator" 100 1
              ; transfer_arg_of_names "owner2" "operator" 10 1
              ; Fmt.str "--source %S" (sk_of_name "operator")
              ; Fmt.str "--address %s" contract_address_arg ]
          ; par
              "We can then observe the resulting state where all the balances \
               are `0` except for `operator` who owns the total supply:"
          ; show_contract ~also:["operator"] [0; 1; 2] ]
      ; section "Retrieve The Contract's Balance"
          [ par
              "The build of the contract we originated above has an extra \
               entry-point to be able to transfer the balance of the contract, \
               e.g. in case somebody accidentally transfers μꜩ to the \
               contract."
          ; par
              "So let's imagine than after the above heist, `operator` wants \
               to publicly tip/bribe the contract's administrator(s) by going \
               through the contract itself (this may be a convoluted excuse to \
               put XTZ on the contract …). We call the `transfer` \
               entry-point with an empty list of transfer-items but with a few \
               XTZ as amount:"
          ; command_bloc ~hidden_setup
              ~how:(`Code_block [`Command; `Stdout (Some (0, 8))])
              (String.concat ~sep:"\n"
                 [ indented_command "tezos-client"
                     [ "import secret key operator"
                     ; Fmt.str "%S --force" (sk_of_name "operator") ]
                 ; indented_command "tezos-client"
                     [ "transfer 1_000 from operator"
                     ; Fmt.str "to %s" contract_address_arg
                     ; "--entrypoint transfer"; "--arg '{}' --burn-cap 1" ] ])
          ; par
              "We see that `fatoo` shows a non-zero balance for the contract \
               now:"; show_contract []
          ; par
              "Let's make `admin` retrieve that money for themselves; the \
               entry-point is called `mutez_transfer` and takes a pair `mutez \
               × address`:"
          ; command_bloc ~hidden_setup
              ~how:(`Code_block [`Command; `Stdout (Some (0, 8))])
              (String.concat ~sep:"\n"
                 [ indented_command "tezos-client"
                     [ "import secret key admin"
                     ; Fmt.str "%S --force" (sk_of_name "admin") ]
                 ; indented_command "tezos-client"
                     [ "transfer 0 from admin"
                     ; Fmt.str "to %s" contract_address_arg
                     ; "--entrypoint mutez_transfer"
                     ; Fmt.str "--arg \"Pair 1000000000 \\\"%s\\\"\""
                         (pkh_of_name "admin"); "--burn-cap 1" ] ])
          ; par "We see that the balance is gone from the KT1:"
          ; show_contract []; par "… and see that `admin` is wealthier:"
          ; command_bloc ~how:command_and_stdall ~hidden_setup
              (String.concat ~sep:"\n"
                 [ indented_command "tezos-client"
                     ["get balance for"; Fmt.str "%S" (pkh_of_name "admin")] ])
          ] ] in
  make ~header
    [ par
        ( "This tutorial shows how to interact with the “FA2-SmartPy” \
           implementation of the FA2 standard on some common use-cases."
        %% "The first part uses `tezos-client` commands to operate basic \
            transfers and queries."
        %% "The second part goes further: it uses the `fatoo` command line \
            interface to do batched-transfers and use the “operator” \
            mechanism to delegate transfer rights." )
    ; ( if skip_basic_section then par "**SKIPPED BASIC SECTION**"
      else basic_section () ); fatoo_section ()
    ; section "Further Reading"
        [ Fmt.kstr par
            "Hopefully this tutorial introduced the FA2-SmartPy implementation \
             of FA2 from a user's perspective. Please provide any feedback \
             using the repository's [issues](%s). Further reading includes:"
            fatoo_gitlab_issues
        ; itemize
            [ Fmt.str "the TZIP-12 [specification](%s) itself;" fa2_spec_md
            ; Fmt.str "the implementation source code [`multi_asset.py`](%s);"
                fatoo_multi_asset_dot_py
            ; Fmt.str "the Agora (blog) [post](%s) introducing the project;"
                intro_agora_post; "🚧 and more to come … 👷" ] ] ]

let run ~header ?skip_basic_section ?output state () =
  let with_ppf f =
    match output with
    | None -> f Fmt.stdout
    | Some s ->
        Stdio.Out_channel.with_file s ~f:(fun o ->
            let ppf =
              Caml.Format.(
                let p = formatter_of_out_channel o in
                pp_set_margin p 80 ; p) in
            f ppf) in
  let naked = make_tutorial ~header ?skip_basic_section () in
  Markdown.complete state naked
  >>= fun filled ->
  with_ppf (fun ppf -> Markdown.print ppf filled ; Fmt.pf ppf "\n%!") ;
  Io.return ()

let default_header = [("id", "fatooturial"); ("title", "The Fatootorial")]

let cmd () =
  let open Cmdliner in
  let open Term in
  ( const (fun state header skip_basic_section output () ->
        Io.run (fun () -> run ~header ?skip_basic_section ?output state ()))
    $ State.cmdliner_term ()
    $ Arg.(
        value
          (opt_all
             (pair ~sep:':' string string)
             default_header
             (info ["header"; "H"]
                ~doc:"Add pieces of header to the markdown header.")))
    $ Arg.(
        value
          (opt (some bool) None
             (info ["skip-basic-section"] ~doc:"Skip the first big section.")))
    $ Arg.(
        value
          (opt (some string) None (info ["output"; "o"] ~doc:"Output path.")))
    $ const ()
  , info "show-tutorial" ~doc:"Dump development version of the tutorial." )
