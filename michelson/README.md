Generated Michelson Contracts
=============================


Latest builds as pure Michelson files:

* `20200615-162614+0000_e1e6c44_contract.tz`: The default.
* `20200615-162614+0000_e1e6c44_dbg_contract.tz`: The default in debug mode.
* `20200615-162614+0000_e1e6c44_mutran_contract.tz`: The default with mutez transfer entry-point.
* `20200615-162614+0000_e1e6c44_perdesc_noops_contract.tz`: The default without operators and with permissions-descriptor.
* `20200615-162614+0000_e1e6c44_single_mutran_contract.tz`: The single-asset with mutez transfer entry-point.
* `20200615-162614+0000_e1e6c44_nft_mutran_contract.tz`: The default in NFT mode with mutez transfer entry-point.


See also `fatoo list-contract-variants --help` for all available builds and details about them:


* `contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_contract`
    * `description`: The default.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `dbg_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_dbg_contract`
    * `description`: The default in debug mode.
    * `debug`: ✅
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `baby_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_baby_contract`
    * `description`: The default in Babylon mode.
    * `debug`: ⛌
    * `carthage_pairs`: ⛌
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `nolay_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_nolay_contract`
    * `description`: The default without right-combs.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ⛌
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `mutran_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_mutran_contract`
    * `description`: The default with mutez transfer entry-point.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `tokset_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_tokset_contract`
    * `description`: The default with non-consecutive token-IDs.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ⛌
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `perdesc_noops_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_perdesc_noops_contract`
    * `description`: The default without operators and with permissions-descriptor.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ⛌
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ✅
* `perdesc_noops_dbg_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_perdesc_noops_dbg_contract`
    * `description`: The perdesc_noops_contract but in debug mode.
    * `debug`: ✅
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ⛌
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ⛌
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ✅
* `single_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_single_contract`
    * `description`: The default for single-asset.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ⛌
    * `single_asset`: ✅
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `single_mutran_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_single_mutran_contract`
    * `description`: The single-asset with mutez transfer entry-point.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ✅
    * `non_fungible`: ⛌
    * `add_permissions_descriptor`: ⛌
* `nft_mutran_contract`:
    * `version_string`: `version_20200615_tzip_a57dfe86_nft_mutran_contract`
    * `description`: The default in NFT mode with mutez transfer entry-point.
    * `debug`: ⛌
    * `carthage_pairs`: ✅
    * `force_layouts`: ✅
    * `support_operator`: ✅
    * `assume_consecutive_token_ids`: ✅
    * `add_mutez_transfer`: ✅
    * `single_asset`: ⛌
    * `non_fungible`: ✅
    * `add_permissions_descriptor`: ⛌
