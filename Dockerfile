# Use this like:
# $ docker build -t <imagename> --target <setup|build|run> . \
#  --build-arg flextesa_build_image --build-arg smartpy_version

ARG flextesa_build_image

# Stage 1: setup dockerimage
FROM $flextesa_build_image as setup
ARG smartpy_version
RUN sudo apk add net-tools jq rlwrap parallel nodejs npm python3
RUN curl -s https://SmartPy.io/$smartpy_version/SmartPy.sh -o /tmp/SmartPy.sh
RUN sh /tmp/SmartPy.sh local-install-custom https://SmartPy.io/$smartpy_version /tmp/install-smpy
WORKDIR /usr/bin
RUN sudo sh -c "mv /tmp/install-smpy/* ."
RUN opam install -y base fmt uri cmdliner ezjsonm ocamlformat uri merlin ppx_deriving angstrom

# Stage 2: build dockerimage
FROM setup as build
RUN sudo mkdir -p /buildfa2
RUN sudo chown -R opam:opam /buildfa2
WORKDIR /buildfa2
ADD --chown=opam:opam  ./dune-project ./please.sh ./multi_asset.py ./
ADD --chown=opam:opam  ./src ./src
RUN find .
RUN ./please.sh build
RUN sudo ./please.sh install

# Stage 3: run dockerimage
FROM alpine as run
COPY --from=build /usr/bin/flextesa /usr/bin/flextesa
COPY --from=build /usr/bin/tezos-node /usr/bin/tezos-node
COPY --from=build /usr/bin/tezos-client /usr/bin/tezos-client
COPY --from=build /usr/bin/tezos-admin-client /usr/bin/tezos-admin-client
COPY --from=build /usr/bin/tezos-validator /usr/bin/tezos-validator
COPY --from=build /usr/bin/tezos-signer /usr/bin/tezos-signer
COPY --from=build /usr/bin/tezos-codec /usr/bin/tezos-codec
COPY --from=build /usr/bin/tezos-protocol-compiler /usr/bin/tezos-protocol-compiler
COPY --from=build /usr/bin/tezos-baker-alpha /usr/bin/tezos-baker-alpha
COPY --from=build /usr/bin/tezos-endorser-alpha /usr/bin/tezos-endorser-alpha
COPY --from=build /usr/bin/tezos-accuser-alpha /usr/bin/tezos-accuser-alpha
COPY --from=build /usr/bin/fatoo /usr/bin/fatoo
# Stolen from Flextesa:
RUN sh -c "echo '@testing http://nl.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories"
RUN sh -c "echo '@testing http://nl.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories"
RUN apk update
RUN apk add curl net-tools rlwrap@testing gmp-dev hidapi-dev@testing m4 perl pkgconfig libev-dev