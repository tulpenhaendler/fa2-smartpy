#!/bin/sh

set -e

export flextesa_commit=d33f98de

get_build_envs() {
  echo "export flextesa_commit=$flextesa_commit"
  echo "export flextesa_build_image=registry.gitlab.com/tezos/flextesa:$flextesa_commit-build"
  echo "export smartpy_version=$(./please.sh get_smartpy_recommended_version)"
}

quicktest () {
    case "$1" in 
        "setup" )
            docker run -v "$PWD:/work" --rm $2 sh -c 'cd /work ; ./please.sh check'
            ;;
        "build" | "run" )
            docker run -v "$PWD:/work" --rm $2 sh -c 'cd /work ; fatoo --help=plain'
            ;;
        * )
            echo WUT ; exit 2 ;;
    esac
}

shell (){
    docker run -v "$PWD:/work" --rm -it fa2:test sh
}

"$@"
